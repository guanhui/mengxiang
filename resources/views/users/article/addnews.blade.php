@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">添加图文内容</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="{{ url('/user/article/create') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="type" value="3">
                        <input type="hidden" name="token" value="{{ $token }}">

						<div class="form-group">
							<label class="col-md-4 control-label">关键字</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="keyword" value="">
							</div>
						</div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">标题</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="title" value="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">作者</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="author" value="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">图片地址</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="img" value="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">简介</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="remark" value="">
                            </div>
                        </div>

						<div class="form-group">
							<label class="col-md-4 control-label">内容</label>
							<div class="col-md-6">
								<textarea class="form-control" rows="10" name="content"></textarea>
							</div>
						</div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">原文链接</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="url" value="">
                            </div>
                        </div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									保存
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
