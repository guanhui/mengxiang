<style>
    table{
        width: 100%;
    }

    table tr{
        border-bottom: 1px solid #e8e8e8;
    }

    table tr th{
        padding: 10px 0;
        text-align: center;
        background: #e5e5e5;
        font-size: 14px;
        border-right: 1px solid #d5d5d5;
    }

    table tr td{
        padding: 10px 0;
        font-size: 12px;
        border-right: 1px solid #d5d5d5;
        text-align: center;
    }

    .imgtxt,.txt{
        float:right;
        margin-left: 5px;
    }
</style>
@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">文章列表<span class="imgtxt"><a href="{{ url('user/article/show',array(3)) }}">添加图文</a></span><span class="txt"><a href="{{ url('user/article/show',array(1)) }}">添加文本</a></span></div>
				<div class="panel-body" style="padding: 0">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <th>关键字</th>
                            <th>标题</th>
                            <th>作者</th>
                            <th>类型</th>
                            <th>创建时间</th>
                            <th>操作</th>
                        </tr>
                        @foreach($list as $val)
                            <tr>
                                <td>{{ $val->keyword }}</td>
                                <td>{{ $val->title }}</td>
                                <td>{{ $val->author }}</td>
                                <td>{{ $val->type }}</td>
                                <td>{{ $val->created_at }}</td>
                                <td>
                                    <a href="{{ url('/user/article/edit', array($val->type,$val->id)) }}">修改</a>
                                    <a href="{{ url('/user/article/destroy',array($val->id)) }}">删除</a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
