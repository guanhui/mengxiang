@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">添加文本消息</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="{{ url('/user/article/update') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="id" value="{{ $result['id'] }}">
                        <input type="hidden" name="type" value="1">

						<div class="form-group">
							<label class="col-md-4 control-label">关键字</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="keyword" value="{{ $result['keyword'] }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">内容</label>
							<div class="col-md-6">
								<textarea class="form-control" name="content">{{ $result['content'] }}</textarea>
							</div>
						</div>


						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									修改
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
