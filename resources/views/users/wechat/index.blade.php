<style>
    table{
        width: 100%;
    }

    table tr{
        border-bottom: 1px solid #e8e8e8;
    }

    table tr th{
        padding: 10px 0;
        text-align: center;
        background: #e5e5e5;
        font-size: 14px;
        border-right: 1px solid #d5d5d5;
    }

    table tr td{
        padding: 10px 0;
        font-size: 12px;
        border-right: 1px solid #d5d5d5;
        text-align: center;
    }
</style>
@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">我的公众号</div>
				<div class="panel-body" style="padding: 0">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <th>公众号名称</th>
                            <th>VIP等级</th>
                            <th>创建时间/到期时间</th>
                            <th>已定义/上限</th>
                            <th>请求数</th>
                            <th>操作</th>
                        </tr>
                        @foreach($list as $val)
                        <tr>
                            <td>{{ $val->name }}</td>
                            <td>{{ $val->name }}</td>
                            <td>{{ $val->created_at }}</td>
                            <td>{{ $val->name }}</td>
                            <td>{{ $val->name }}</td>
                            <td>
                                <a href="{{ url('/user/article',array($val->token)) }}">功能管理</a>
                                <a href="{{ url('/user/wechat/token',array($val->token)) }}">API接口</a>
                            </td>
                        </tr>
                        @endforeach
                    </table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
