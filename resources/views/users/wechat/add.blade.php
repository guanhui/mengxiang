@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">注册</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="{{ url('/user/wechat/create') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-4 control-label">公众号名称</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="name" value="{{ old('name') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">公众号原始id</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="first_id" value="{{ old('first_id') }}">
							</div>
						</div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">微信号</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="wechat_id" value="{{ old('wechat_id') }}">
                            </div>
                        </div>

						<div class="form-group">
							<label class="col-md-4 control-label">AppID</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="appid">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">AppSecret</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="appsecret">
							</div>
						</div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">微信号类型</label>
                            <div class="col-md-6">
                                <select name="type" class="form-control">
                                    <option value="1">订阅号</option>
                                    <option value="2">服务号</option>
                                    <option value="3">企业号</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">简介</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="remark">
                            </div>
                        </div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									添加
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
