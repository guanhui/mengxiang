<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWechatsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('wechats', function(Blueprint $table)
		{
            $table->increments('id');
            $table->integer('user_id');
            $table->string('name');
            $table->string('wechat_id');
            $table->string('type');
            $table->string('appid');
            $table->string('appsecret');
            $table->string('remark');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('wechats');
	}

}
