<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('articles', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('keyword');  //关键字
            $table->string('token');  //token 表示公众号
            $table->text('content');  //内容
            $table->string('title');  //标题
            $table->tinyInteger('type');   //类型
            $table->string('author');   //作者
            $table->string('remark'); //摘要
            $table->string('url');   //原文链接
            $table->string('img');   //封面
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('articles');
	}

}
