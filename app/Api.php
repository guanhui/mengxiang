<?php namespace App;
/**
 * 微信消息接口
 */
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Api extends Model {

    protected $token;

    public function __construct($token)
    {
        $this->token = $token;
    }

    public function valid()
    {
        $echoStr = $_GET["echostr"];
        //valid signature , option
        if($this->checkSignature()){
            echo $echoStr;
            exit;
        }
    }

    /**
     * 签名认证
     * @return bool
     */
    private function checkSignature()
    {
        $signature = $_GET["signature"];
        $timestamp = $_GET["timestamp"];
        $nonce = $_GET["nonce"];
        $token = $this->token;
        $tmpArr = array($token, $timestamp, $nonce);
        sort($tmpArr, SORT_STRING);
        $tmpStr = implode( $tmpArr );
        $tmpStr = sha1( $tmpStr );

        if( $tmpStr == $signature ){
            return true;
        }else{
            return false;
        }
    }

    /**
     * 消息接收回复
     */
    public function responseMsg()
    {
        //get post data, May be due to the different environments
        $postStr = $GLOBALS["HTTP_RAW_POST_DATA"];

        //extract post data
        if (!empty($postStr)) {

            $postObj = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);

            $RX_TYPE = trim($postObj->MsgType);

            if (!empty($RX_TYPE)) {

                $result = $this->MessageCotent($postObj);
                echo $result;
            } else {

                echo '';
                exit;
            }
        }

    }

    /**
     * 文本内容组装
     * @param $postObj
     * @return string
     */
    public function MessageCotent($postObj)
    {
        $res = $this->getData($postObj->Content, $postObj->MsgType);

        if(empty($res)) return;

        switch(trim($postObj->MsgType))
        {
            case 'text':
                $result = $this->responseText($postObj, $res[0]['content']);
                break;
            case 'news':
                $result = $this->responseNews($postObj, $res);
                break;
            case 'image':
                $result = $this->responseImg($postObj);
        }
        return $result;
    }

    /**
     * 文本回复
     * @param $postObj
     * @param $content
     * @return string
     */
    public function responseText($postObj, $content)
    {
        $textTpl = "<xml>
                    <ToUserName><![CDATA[%s]]></ToUserName>
                    <FromUserName><![CDATA[%s]]></FromUserName>
                    <CreateTime>%s</CreateTime>
                    <MsgType><![CDATA[%s]]></MsgType>
                    <Content><![CDATA[%s]]></Content>
                    <FuncFlag>0</FuncFlag>
                    </xml>";
        $resultStr = sprintf($textTpl, $postObj->FromUserName, $postObj->ToUsername, time(), $postObj->MsgType, $content);
        return $resultStr;
    }

    /**
     * 图文回复
     * @param $postObj
     * @param $content
     * @return string|void
     */
    public function responseNews($postObj, $content)
    {
        if(!is_array($content)) return;

        $itemTpl = "<item>
            <Title><![CDATA[%s]]></Title>
            <Description><![CDATA[%s]]></Description>
            <PicUrl><![CDATA[%s]]></PicUrl>
            <Url><![CDATA[%s]]></Url>
            </item>";
        $items = '';
        foreach($content as $key=>$val)
        {
            $items .= sprintf($itemTpl, $val['title'], $val['remark'], $val['img'], $val['url']);
        }

        $newsTpl = "<xml>
                    <ToUserName><![CDATA[%s]]></ToUserName>
                    <FromUserName><![CDATA[%s]]></FromUserName>
                    <CreateTime>%s</CreateTime>
                    <MsgType><![CDATA[news]]></MsgType>
                    <ArticleCount>%s</ArticleCount>
                    <Articles>$items</Articles>
                    ";

        $result = sprintf($newsTpl, $postObj->FromUserName, $postObj->ToUserName, time(), count($content));
        return $result;
    }

    /**
     * 回复图片
     * @param $postObj
     * @return string
     */
    public function responseImg($postObj)
    {
        $imgTpl = "<xml>
                <ToUserName><![CDATA[%s]]></ToUserName>
                <FromUserName><![CDATA[%s]]></FromUserName>
                <CreateTime>%s</CreateTime>
                <MsgType><![CDATA[image]]></MsgType>
                <Image>
                <MediaId><![CDATA[%s]]></MediaId>
                </Image>
                </xml>";
        $result = sprintf($imgTpl, $postObj->FromUserName, $postObj->ToUserName, time(), $postObj->MediaId);
        return $result;
    }

    /**
     * 从数据库中获取回复内容
     * @param $keyword
     * @param string $type
     * @return mixed
     */
    public function getData($keyword, $type='text')
    {
        $atype = $this->getArticleType($type);
        $result = DB::select('select * from articles where type=? and keyword=?', [$atype, $keyword]);
        return $result;
    }

    /**
     * 获取文章类型
     * @param $type
     */
    public function getArticleType($type)
    {
        switch($type)
        {
            case 'text':
                $result = 1;
                break;
            case 'image':
                $result = 2;
                break;
            case 'news':
                $result = 3;
                break;
        }
        return $result;
    }
}
