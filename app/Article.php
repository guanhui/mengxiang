<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model {

    protected $guarded = ['id'];  //不可以批量赋值

    protected $fillable = ['keyword', 'content'];
}
