<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('home', 'HomeController@index');

Route::get('api/{token}','ApiController@index');   //消息接口

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);


Route::group(['prefix' => 'user', 'namespace' => 'User'],function(){
    Route::get('/', 'UserController@login');
    Route::get('user', 'UserController@index');
    Route::get('user/login', 'UserController@login');
    Route::get('user/register', 'UserController@register');
    Route::get('user/home', 'UserController@home');
    Route::get('user/logout', 'UserController@logout');
    Route::post('user/postLogin', 'UserController@postLogin');
    Route::post('user/postRegister', 'UserController@postRegister');
});

//需要检测用户是否登录
Route::group(['prefix' => 'user', 'namespace' => 'User', 'middleware' => 'auth'],function(){
    Route::get('wechat', 'WechatController@index');
    Route::post('wechat/create', 'WechatController@create');
    Route::get('wechat/add', 'WechatController@add');
    Route::get('wechat/token/{token}', 'WechatController@token');
  //  Route::resource('article', 'ArticleController');
    Route::get('article/{token}', 'ArticleController@index');
    Route::get('article/show/{id}', 'ArticleController@show');
    Route::post('article/create', 'ArticleController@create');
    Route::get('article/destroy/{id}', 'ArticleController@destroy');
    Route::get('article/edit/{type}/{id}', 'ArticleController@edit');
    Route::post('article/update', 'ArticleController@update');
});


Route::group(['prefix' => 'admin', 'namespace' => 'Admin'],function(){
    Route::get('index', 'LoginController@index');
    Route::get('register', 'LoginController@register');
    Route::post('login/postRegister', 'LoginController@postRegister');
});


