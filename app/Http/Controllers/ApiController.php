<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Api;

class ApiController extends Controller {

    public function index($token)
    {
        $api = new Api($token);

        if(!isset($_GET['echostr']))
        {
            $api->responseMsg();  //认证通过
        }
        else
        {
            $api->valid();
        }

    }
}
