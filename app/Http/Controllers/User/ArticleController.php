<?php namespace App\Http\Controllers\User;
use App\Article;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class ArticleController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($token)
	{
        $list = DB::select('select * from articles where token=?',[$token]);
        Session::put('wechat_token', $token);
       // $list['token'] = $token;
        return view('users.article.index')->with('list', $list);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $article = new Article();
        $article->keyword = Input::get('keyword');
        $article->content = Input::get('content');
        $article->token = Input::get('token');
        $article->type = Input::get('type');
        if(Input::get('type') == 1)
        {
            $article->save();
        }
        else
        {
            $article->title = Input::get('title');
            $article->author = Input::get('author');
            $article->img = Input::get('img');
            $article->url = Input::get('url');
            $article->remark = Input::get('remark');
            $article->save();
        }
        return Redirect::to('user/article/'.Session::get('wechat_token'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        if($id==1)
        {
            return view('users.article.addtxt')->with('token', Session::get('wechat_token'));
        }
        else if($id==3)
        {
            return view('users.article.addnews')->with('token', Session::get('wechat_token'));
        }
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($type,$id)
	{
        $res = Article::find($id);
        if($type==1)
        {
            return view('users.article.edittxt')->with('result', $res);
        }
        else if($type==3)
        {
            return view('users.article.editnews')->with('result', $res);
        }
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
        if(Input::get('type') == 1)
        {
            DB::update('update articles set keyword=?, content=? where id=?',[Input::get('keyword'),Input::get('content'), Input::get('id')]);
        }
        else
        {
            DB::update('update articles set keyword=?, content=?, title=?, author=?, img=?, remark=?, url=? where id=?',[Input::get('keyword'),Input::get('content'), Input::get('title'), Input::get('author'), Input::get('img'), Input::get('remark'),Input::get('url'), Input::get('id')]);
        }

        return Redirect::to('user/article/'.Session::get('wechat_token'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        DB::delete('delete from articles where id=?', [$id]);
        return Redirect::to('user/article/'.Session::get('wechat_token'));
	}

}
