<?php namespace App\Http\Controllers\User;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use App\User;
use Auth;

class UserController extends Controller {

    public function __construct() {
        $this->beforeFilter('csrf', array('on'=>'post'));
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
    {
        return view("users.user.login");
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        return view('user.home');
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

    public function home()
    {
        return view('users.user.home');
    }

    /**
     * 用户登录
     *
     * @return \Illuminate\View\View
     */
    public function login()
    {
        return view('users.user.login');
    }

    public function postLogin()
    {
        if (Auth::attempt(array('email'=>Input::get('email'), 'password'=>Input::get('password')))) {
            if(Auth::user()->status == 1)
            {
                return Redirect::to('admin/index')->with('message', '欢迎登录');
            }
            else
            {
                return Redirect::to('user/wechat')->with('message', '欢迎登录');
            }
        } else {
            return Redirect::to('user/user/login')->with('message', '用户名或密码错误')->withInput();
        }
    }

    /**
     * 用户注册
     *
     * @return \Illuminate\View\View
     */
    public function register()
    {
        return view('users.user.register');
    }

    public function postRegister()
    {
        $user = new User();//实例化User对象
        $user->name = Input::get('name');
        $user->email = Input::get('email');
        $user->tel = Input::get('tel');
        $user->password = Hash::make(Input::get('password'));
        $user->save();
        return Redirect::to('user/login')->with('message', '欢迎注册，好好玩耍!');
    }

    public function logout()
    {
        Auth::logout();
        return Redirect::to('/user/user/login');
    }
}
