<?php namespace App\Http\Controllers\User;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Wechat;
use Auth;


class WechatController extends Controller
{
    public function __construct()
    {

    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public function index()
    {
        $list = DB::table('wechats')->get();
        return view('users.wechat.index')->with('list', $list);
    }

	public function add()
	{
        return view('users.wechat.add');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $wechat = new Wechat();
        $wechat->user_id = Auth::user()->id;
        $wechat->name = Input::get('name');
        $wechat->first_id = Input::get('first_id');
        $wechat->wechat_id = Input::get('wechat_id');
        $wechat->type = Input::get('type');
        $wechat->appid = Input::get('appid');
        $wechat->appsecret = Input::get('appsecret');
        $wechat->remark = Input::get('remark');
        $wechat->token = getoken();
        $wechat->save();
        return Redirect::to('user/wechat')->with('message', '添加成功!');
	}

    public function token($token)
    {
        return view('users.wechat.token')->with('token', $token);
    }

}
