<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Auth;
use App\User;

class LoginController extends Controller {

    public function index()
    {
        echo 'success';
    }


    //注册
    public function register()
    {
        return view('admin.login.register');
    }

    public function postRegister()
    {
        $admin = new User();//实例化User对象
        $admin->name = Input::get('name');
        $admin->email = Input::get('email');
        $admin->tel = Input::get('tel');
        $admin->password = Hash::make(Input::get('password'));
        $admin->status = 1;
        $admin->save();
        return Redirect::to('user')->with('message', '欢迎注册，好好玩耍!');
    }

}
