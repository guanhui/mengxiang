/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50540
Source Host           : 127.0.0.1:3306
Source Database       : laravel5

Target Server Type    : MYSQL
Target Server Version : 50540
File Encoding         : 65001

Date: 2015-04-21 17:23:27
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table', '1');
INSERT INTO `migrations` VALUES ('2014_10_12_100000_create_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('2015_04_18_025347_create_articles_table', '2');
INSERT INTO `migrations` VALUES ('2015_04_18_025411_create_pages_table', '2');
INSERT INTO `migrations` VALUES ('2015_04_21_063509_create_wechats_table', '3');

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tel` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'byron', 'sunbyron@qq.com', null, '$2y$10$XkLEB3tlbKVmTWv2JZwhmebbo7u6mV.3hgq7PhZRuHP71wHR/FP1y', 'wZGMRBvBf3lVoeO1tgV8o6GlHKIpgOGHRlzyhX46nLCwqpAGfJYWWTYsxHLP', '2015-04-18 02:28:16', '2015-04-18 03:12:36');
INSERT INTO `users` VALUES ('2', '苏三', 'sunsan@sina.com', null, '$2y$10$OWoP0sBCQX1KSb9a0til5.GMkgK/DpNW8lbmidFWvQ.SCzWzfQvL2', 'SgyiiN4bvYFrTfTZ7DXKd88Wdiuwi879pXmcCqZtsCesh2OxiXoZpkbNXORn', '2015-04-18 06:07:07', '2015-04-18 06:12:26');
INSERT INTO `users` VALUES ('3', 'yingying', 'yingying@souhu.com', null, '$2y$10$sukzddbakFNYMOp5Jhd4QOLNDDSkz7Pa6EjGhocseXfm9VZwh4mha', 'lRqJziUAX2gm1jvOzz4jCtkuFBJ3J1hQvWAw2DeB047jkQgmfhuv2TGPlp1v', '2015-04-18 06:13:11', '2015-04-18 06:14:01');
INSERT INTO `users` VALUES ('4', 'mx', 'mx@baidu.com', null, '$2y$10$h/kZccMXPIOM3AI0zZHEc.4rdlE0Gemmu33XqPKNo38Fn.qMbhRte', 'VaECt9Bcm7pyIRpC58W2u3zYLIfQLMlBmczaFToonIMiDSTNbLuadB8JfnCe', '2015-04-18 06:14:49', '2015-04-18 06:38:25');
INSERT INTO `users` VALUES ('5', 'annies', 'annies@an.com', null, '$2y$10$TIyc7YIErySyYWezlGyXguuC36GNtRNXDAHkMvyVQ4vBlt3aRqGO2', null, '2015-04-18 06:39:01', '2015-04-18 06:39:01');
INSERT INTO `users` VALUES ('9', 'wade3', 'wade3@tencen.com', '12345678954', '$2y$10$F13RydUm7TH.kPUP1vw9C.ywijCEs5QtyOyCyxQiUWimh2.eWmwja', null, '2015-04-21 06:08:08', '2015-04-21 06:08:08');

-- ----------------------------
-- Table structure for wechats
-- ----------------------------
DROP TABLE IF EXISTS `wechats`;
CREATE TABLE `wechats` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_id` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wechat_id` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` tinyint(1) DEFAULT NULL,
  `appid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `appsecret` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remark` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of wechats
-- ----------------------------
INSERT INTO `wechats` VALUES ('1', '1', '梦享篮子', 'gh_474c630d9c85', 'zgxmy123', '1', '3sdfcsdvcfwrhiunj32', '服务号', '超级管理员，拥有最高权限', '2015-04-21 07:23:35', '2015-04-21 07:23:35');
INSERT INTO `wechats` VALUES ('2', '1', '唯梦空间', 'gh_474c630d9c81', 'weimeng123', '1', '1212321312', 'b0d115d2bad49f7fc4893fcf51fc63a3 ', '受尊重的企业', '2015-04-21 07:28:03', '2015-04-21 07:28:03');
INSERT INTO `wechats` VALUES ('3', '9', '梦新科技', 'gh_474c630d9c83', 'weimeng654', '2', '3sdfcsdvcfwrhiunj98', 'b0d115d2bad49f7fc4893fcf51fc63aw3 ', '超级管理员，拥有最高权限', '2015-04-21 07:36:50', '2015-04-21 07:36:50');
