## 梦享篮子 V1.0
###1、布局
	user（用户后台）  admin（管理后台）  wap(手机端)
###2、用户注册、登录
###3、用户公众号登录

###4、微信开发包

####基本使用
  
<?php
  
use Overtrue\Wechat\Wechat;

$options = [
    'appId'          => 'Your app id',
    'secret'         => 'Your secret'
    'token'          => 'Your token',
    'encodingAESKey' => 'Your encoding AES Key' // optional
];
	
$wechat = Wechat::make($options);

$server = $wechat->on('message', function($message){
    error_log("收到来自'{$message['FromUserName']}'的消息：{$message['Content']}");
});

$result = $wechat->serve();

// 您可以直接echo 或者返回给框架
echo $result;

?>

 	功能
	用户
	 用户组
	 客服
	 监听事件与消息
	 基本消息类型
	 图文消息
	 自定义菜单
	 门店管理
	 Auth



## 梦享篮子 V2.0
###功能
	完成了文章管理和消息回复，用户可以添加文本内容和图文内容到数据库，然后根据关键字回复给用户，图文现在是不能上传图片和还没有富文本编辑器。
	微信的消息接口是app目录下的Api.php，只要传入token值，就可以实现验、消息接收和回复
	使用demo
	use App\Api;
	$api = new Api($token);
    if(!isset($_GET['echostr']))
    {
        $api->responseMsg();  //认证通过
    }
    else
    {
        $api->valid();
    }
